package com.kazan.view;

import com.kazan.controller.Controller;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class MyView {

    Scanner scanner = new Scanner(System.in);
    Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    public MyView() {
        controller = new Controller();
        menu = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();

        menu.put("1", "1 - Task 1 - PingPong");
        menu.put("2", "2 - Task 2 - Fibonacci sequence with runnable");
        menu.put("3", "3 - Task 3 - Fibonacci sequence with executors");
        menu.put("4", "4 - Task 4 - Get sum of fibonacci sequence with callable");
        menu.put("Q", "Q - Exit");

        menuMethods.put("1", this::doFirstTask);
        menuMethods.put("2", this::doSecondTask);
        menuMethods.put("3", this::doThirdTask);
        menuMethods.put("4", this::doFourthTask);
    }



    private void doFirstTask() {
        controller.doFirstTask();
    }

    private void doSecondTask() {
        controller.doSecondTask();
    }

    private void doThirdTask() {
        controller.doThirdTask();
    }

    private void doFourthTask() {
        try {
            controller.doFourthTask();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private void outputMenu() {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public void start() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Please, select menu point: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuMethods.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
        scanner.close();
        System.exit(0);
    }

}
