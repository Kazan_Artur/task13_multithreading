package com.kazan.controller;

import com.kazan.model.task1.PingPong;
import com.kazan.model.task2.Fibonacci;
import com.kazan.model.task3.FibonacciExecutor;
import com.kazan.model.task4.FibonacciCallable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class Controller {

    private static Logger log = LogManager.getLogger(Controller.class);
    Random random = new Random();
    PingPong pingPong;


    public Controller() {
        pingPong = new PingPong();


    }

    public void doFirstTask() {
        pingPong.show();
        try {
            TimeUnit.MILLISECONDS.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pingPong.setFlag(false);
    }

    public void doSecondTask() {
        List<Fibonacci> fibonacciList = new ArrayList<>();
        List<Thread> threadList = new ArrayList<>();
        final int threads = 4;
        for (int i = 0; i < threads; i++) {
            fibonacciList.add(new Fibonacci(random.nextInt(10) + 2));
        }
        for (int i = 0; i < threads; i++) {
            threadList.add(new Thread(fibonacciList.get(i)));
            fibonacciList.get(i).run();
        }
        for (int i = 0; i < threads; i++) {
            log.info(threadList.get(i).getName());
            log.info(Arrays.toString(fibonacciList.get(i).getSequence()));
        }
    }

    public void doThirdTask() {
        FibonacciExecutor fibonacci1 = new FibonacciExecutor(random.nextInt(10) + 2);
        FibonacciExecutor fibonacci2 = new FibonacciExecutor(random.nextInt(10) + 4);
        FibonacciExecutor fibonacci3 = new FibonacciExecutor(random.nextInt(10) + 6);
        FibonacciExecutor fibonacci4 = new FibonacciExecutor(random.nextInt(10) + 6);
        FibonacciExecutor fibonacci5 = new FibonacciExecutor(random.nextInt(10) + 6);
        FibonacciExecutor fibonacci6 = new FibonacciExecutor(random.nextInt(10) + 6);

        ExecutorService fixedExecutor = Executors.newFixedThreadPool(3);
        log.info("FixedThreadPool starts");
        fixedExecutor.execute(fibonacci1);
        fixedExecutor.execute(fibonacci2);
        fixedExecutor.execute(fibonacci3);
        fixedExecutor.shutdown();
        log.info("FixedThreadPool ends");

        ExecutorService singleExecutor = Executors.newSingleThreadExecutor();
        log.info("SingleThreadExecutor starts");
        singleExecutor.execute(fibonacci4);
        singleExecutor.execute(fibonacci5);
        singleExecutor.execute(fibonacci6);
        singleExecutor.shutdown();
        log.info("SingleThreadExecutor ends");

        ExecutorService scheduledExecutor = new ScheduledThreadPoolExecutor(3);
        log.info("ScheduledThreadPoolExecutor starts");
        scheduledExecutor.execute(fibonacci1);
        scheduledExecutor.execute(fibonacci2);
        scheduledExecutor.execute(fibonacci3);
        scheduledExecutor.shutdown();
        log.info("ScheduledThreadPoolExecutor ends");
    }

    public void doFourthTask() throws ExecutionException, InterruptedException {
        ExecutorService fixedExecutor = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 10; i++) {
            Future<Integer> future = fixedExecutor.submit(
                    new FibonacciCallable(random.nextInt(10) + 2));
            log.info(future.get());
        }
        Thread.yield();
        fixedExecutor.shutdown();
    }
}
