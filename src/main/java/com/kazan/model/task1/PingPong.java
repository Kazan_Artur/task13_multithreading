package com.kazan.model.task1;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {

    private static final Object object = new Object();
    private static Logger log = LogManager.getLogger(PingPong.class);
    private volatile boolean flag = true;

    public void show() {
        log.info("show start");
        Thread ping = new Thread(
                () -> {
                    synchronized (object) {
                        while (flag) {
                            try {
                                object.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            log.info("Ping");
                            object.notify();
                        }
                    }
                }
        );

        Thread pong = new Thread(
                () -> {
                    synchronized (object) {
                        while (flag) {
                            object.notify();
                            try {
                                object.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            log.info("Pong");
                        }
                    }

                }
        );
        ping.start();
        pong.start();
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

}
